/*
 * (c) Éric Seigne <eric.seigne@cap-rel.fr> - 2019 - GNU AGPLv3
*/

var globalDB = null;                    // Le connecteur vers la base de données SQLite
var databaseEngine = 'IndexedDB';
var databaseName = 'buddyteamlogDB';
var databaseVersion = 1;
var globalIndexedDB = null;               // Le connecteur vers la base de données IndexedDB

