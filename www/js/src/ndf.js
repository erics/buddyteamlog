/*
 * (c) Éric Seigne <eric.seigne@cap-rel.fr> - 2019 - GNU AGPLv3
*/

ons.ready(function () {
  myDebug("ONS Ready, ready in ndf.js");

  // -----------------------------
  // gestion du splitter ...
  let onsleftmenu = document.getElementById('leftmenu');
  if (onsleftmenu != undefined) {
    onsleftmenu.load("parts/splitter.html");
    onsleftmenu.close();
  }

  window.fn = {};

  // =================== migration vers le navigator
  //Ne doit être utilisé qu'une seule fois dans index.html
  //tous les autres appels devraient être gotoPage
  window.fn.load = function (page, data) {
    var content = document.getElementById('globalMyNavigator');
    var onsleftmenu = document.getElementById('leftmenu');
    content.pushPage(page, data)
      .then(onsleftmenu.close.bind(onsleftmenu));
  };

  window.fn.pop = function () {
    var content = document.getElementById('globalMyNavigator');
    content.popPage();
  };

  window.fn.open = function () {
    myDebug("fn.open");

    let onsleftmenu = document.getElementById('leftmenu');
    if (onsleftmenu != undefined)
      onsleftmenu.open();
    else
      myDebug("fn.open : UNDEFINED");
  };
});


//Tout le code pour les pages
document.addEventListener('init', function (event) {
  var objets = "";
  let lapage = event.target.id; //;event.target.pushedOptions.page;

  myDebug("addEventListener : " + lapage);

  //Dans le cas où on est sur l'index / accueil / lancement initial  on ne fait rien
  if (lapage == "loadingPage" || lapage == "ONSSplitter") {
    return;
  }

  translateUI();

  if (lapage == 'ONSApropos') {
    // setPageTitle("À propos");
    feedBuddyTeamLogInfos();
    feedAproposCustom();
    //apres des feed il faut refaire une traduction
    return;
  }

  // ------------- menu
  if (lapage == 'ONSMenu') {
  }
}, false);
