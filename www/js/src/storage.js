/*
 * (c) Éric Seigne <eric.seigne@cap-rel.fr> - 2019-2021 - GNU AGPLv3
*/

/**
 * Ajout d'une entrée dans le stockage local
 *
 * @param   {[type]}  liste  [liste description]
 * @param   {[type]}  entry  [entry description]
 *
 * @return  {[type]}         [return description]
 */
function addLocalStorageEntry(liste, entry) {
  if ((typeof liste != 'undefined') && (liste !== null) && (liste != "")) {
    if ((typeof entry != 'undefined') && (entry !== null) && (entry != "")) {
      // myDebug('Appel de la sauvegarde de la liste des objets pour ...' + liste);
      let listeContent = JSON.parse(localGetData(liste));
      if (Array.isArray(listeContent)) {
        listeContent.push(entry);
        listeContent = myUniqueSort(listeContent);
      }
      else {
        listeContent = new Array(entry);
      }
      localStoreData(liste, JSON.stringify(listeContent));
    }
  }
}


/**
 * Ajout ou remplace une donnée dans la base de données
 * (stockage persistant)
 *
 * @param   {[type]}  lakey    [lakey description]
 * @param   {[type]}  lavalue  [lavalue description]
 *
 * @return  {[type]}           [return description]
 */
function localStoreData(lakey, lavalue) {
  myDebug('localStoreData : ' + lakey + ' et ' + lavalue);
  localStorage.setItem(lakey, lavalue);

  //Si c'est un véhicule on garde la date de dernière mise à jour locale pour
  //pouvoir faire une sync serveur si nécessaire
  if (lakey == "vehicules" || lakey == "vehiculesPro") {
    let ladate = new Date();
    let lastUpdate = ladate.toISOString();
    localStorage.setItem("vehiculesLastUpdate", lastUpdate);
  }

  // myDebug('localStoreData : ' + key + ' et ' + value);
  //Pas la peine de stocker ça en SQL
  if (lakey == 'history') {
    return;
  }

  //shortcut
  if (databaseEngine != 'IndexedDB') {
    return;
  }
  if (globalIndexedDB == null) {
    myDebug('  localStoreData : bdd non connectée, return');
    //On est dans les appels avant que la connexion bdd ne soit active
    return;
    // // myDebug("SQL : On essaye d'ouvrir la base de données ...");
    // // if (databaseEngine == 'IndexedDB') {
    // let openRequest = window.indexedDB.open(databaseName, databaseVersion);
    // openRequest.onerror = function (event) {
    //   myDebug(openRequest.errorCode);
    //   myDebug("global db type IndexedDB re-open error");
    // };
    // openRequest.onsuccess = function (event) {
    //   // Database is open and initialized - we're good to proceed.
    //   globalIndexedDB = openRequest.result;
    //   myDebug("global db type IndexedDB re-open ok");
    //   localStoreDataNext(lakey, lavalue);
    // };
  }
  else {
    localStoreDataNext(lakey, lavalue);
  }
}

function localStoreDataNext(lakey, lavalue) {
  myDebug('  localStoreDataNext : ' + lakey + ' et ' + lavalue);
  if (globalIndexedDB !== null && databaseEngine == 'IndexedDB') {
    // if (databaseEngine == 'IndexedDB') {
    myDebug("  localStoreData put data asked for " + lakey + ":" + lavalue);

    if ((typeof lavalue != 'undefined') && (lavalue !== null) && (lavalue != "")) {
      myDebug("  localStoreData on a une valeur non nulle a stocker : " + lavalue);
    }
    else {
      myDebug("  localStoreData on a une valeur vide ou nulle a stocker");
      lavalue = "none";
    }

    //Tout sur une ligne pour eviter le pb des transactions qui se chevauchent...
    let transaction = globalIndexedDB.transaction(['btlConfig'], "readwrite").objectStore('btlConfig').put({ key: lakey, data: lavalue });
  }
  else {
    myDebug("  ERR: Accès à globalIndexedDB impossible (1) pour stocker " + lakey + " val " + lavalue + " *** ");
  }
}

/**
 * Récupère une donnée locale
 *
 * @param   {[type]}  key  [key description]
 *
 * @return  {[type]}       [return description]
 */
function localGetData(key) {
  if (
    (typeof localStorage.getItem(key) != 'undefined')
    && (localStorage.getItem(key) !== null)
    && (localStorage.getItem(key) != null)
    && (localStorage.getItem(key) != 'null')
    && (localStorage.getItem(key) != "")
    && (localStorage.getItem(key) != undefined)
    && (localStorage.getItem(key) != 'undefined')
  ) {
    let v = localStorage.getItem(key);
    myDebug('localGetData : ' + key + ' -> ' + v);
    if (v == "none") {
      v = "";
    }
    return v;
  }
  myDebug("localGetData: on retourne null pour " + key)
  return null;
}
