/*
 * (c) Éric Seigne <eric.seigne@cap-rel.fr> - 2019 - GNU AGPLv3
*/

/**
 * Fonction de debug locale qui permet de passer en mode silent en prod
 *
 * @param   {string}  msg  message de debug
 *
 * @return  {[type]}       [return description]
 */
function myDebug(msg) {
    if (globalDevMode) {
        if ("object" == typeof msg) {
            console.log('NDFDEBUG : ' + JSON.stringify(msg));
        }
        else {
            console.log('NDFDEBUG : ' + msg);
        }
    }
}

//basename d'un fichier
function basename(path) {
    return path.replace(/\\/g, '/').replace(/.*\//, '');
}

//dirname d'un fichier
function dirname(path) {
    return path.replace(/\\/g, '/').replace(/\/[^\/]*$/, '');;
}
