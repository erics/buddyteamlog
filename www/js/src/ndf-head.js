/*
 * (c) Éric Seigne <eric.seigne@cap-rel.fr> - 2019 - GNU AGPLv3
*/

ons.ready(function () {
  // on capture le bouton back de l'OS
  ons.enableDeviceBackButtonHandler();

  // Set a new handler
  ons.setDefaultDeviceBackButtonListener(function (event) {
    fn.back();
  });

  var path = window.location.pathname;
  var page = path.split("/").pop();
});
