/*
 * (c) Éric Seigne <eric.seigne@cap-rel.fr> - 2019 - GNU AGPLv3
*/

//Demande si on quitte l'appli ...
function askQuitOrGoto(destination) {
    // myDebug('askQuitOrMenu, titre en cours = ' + globalCurrentTitle + " / " + globalCurrentPage);
    // if (globalCurrentTitle == "BuddyTeamLog") {
    if (device !== undefined) {
        if (device.platform == "iOS") {
            gotoPage(destination, false);
        }
        else {
            let optionsNotif = { buttonLabels: ["Oui", "Non"], cancelable: true, title: "Confirmation" };
            ons.notification.confirm("Session fermée.\nVoulez-vous quitter l'application ?", optionsNotif)
                .then(function (index) {
                    if (index === 0) {
                        if (device.platform == "Android") {
                            navigator.app.exitApp();
                        }
                        else {
                            // globalCurrentTitle = "BuddyTeamLog";
                            gotoPage(destination, false);
                        }
                    }
                    else {
                        // globalCurrentTitle = "BuddyTeamLog";
                        gotoPage(destination, false);
                    }
                });
        }
    }
    else {
        myDebug("device is undef !");
    }
    // else {
    //     globalCurrentTitle = "BuddyTeamLog";
    //     gotoPage(destination, false);
    // }
}

//On garde les 20 dernieres étapes de l'historique ...
// -----------------------------
function historique(page) {
    myDebug('historique : ' + page + " > " + globalCurrentPage);
    if (page != globalCurrentPage) {
        globalCurrentPage = page;
        // myDebug('on ajoute à history : ' + page);
        var listeContent = JSON.parse(localGetData("history"));
        // myDebug('history list avant la magie : ' + listeContent);
        if (Array.isArray(listeContent)) {
            //On essaye d'eviter une boucle infinie
            var prevpage = listeContent.pop();
            if (prevpage != page) {
                listeContent.push(prevpage);
            }
            listeContent.push(page);
            //On ne garde que les 20 dernieres pages
            listeContent.splice(0, listeContent.length - 20);
        }
        else {
            listeContent = new Array(page);
        }
        // myDebug('history list apres la magie : ' + listeContent);
        localStoreData("history", JSON.stringify(listeContent));
    }
}

//Ferme la page en cours
//et si refresh = true alors on essaye de faire un refresh de la page en cours ...
function closePage(refresh = false) {
    if (globalMyNavigator !== undefined) {
        myDebug("closePage 2 on est actuellement sur : " + JSON.stringify(globalMyNavigator.topPage.data.name));
        myDebug("closePage 2 on est actuellement sur (variable): " + globalCurrentPage);
        // globalCurrentPage = globalMyNavigator.topPage.name;
        let opt = { animation: 'fade' };
        globalMyNavigator.popPage(opt).then(myDebug("closePage 3 on est actuellement sur : " + JSON.stringify(globalMyNavigator.topPage.data.name)));
    }
    return true;
}

function gotoPage(destination, saveHistory = true) {
    myDebug("gotoPage 1 (" + destination + ") historique (" + saveHistory + ")");
    //Si on est déjà sur la page on ne fait "rien"
    if (globalMyNavigator.topPage != null)
        if (globalMyNavigator.topPage.data.name == destination) {
            return;
        }

    if (globalMyNavigator !== undefined) {
        let changeIsDone = false;

        //Gestion des cas particuliers
        //détails de ndf -> retour sur historique
        if (globalCurrentPage == "details.html" && destination == "historique.html") {
            myDebug("gotoPage : cas particulier details ->historique ");
            let opt = { animation: 'fade' };
            globalMyNavigator.popPage(opt);
            changeIsDone = true;
        }
        if (globalCurrentPage == "detailsLdf.html" && destination == "historique.html") {
            myDebug("gotoPage : cas particulier details ->historique ");
            let opt = { animation: 'fade' };
            globalMyNavigator.popPage(opt);
            changeIsDone = true;
        }
        //modification de véhicule -> retour sur la liste des véhicules
        if ((globalCurrentPage == "config-edVehicule.html" || globalCurrentPage == "config-addVehicule.html" || globalCurrentPage == "config-addVehiculePro.html") && destination == "config.html") {
            myDebug("gotoPage : cas particulier vehicule ");
            let opt = { animation: 'fade' };
            globalMyNavigator.popPage(opt);
            changeIsDone = true;
        }

        globalCurrentPage = destination;
        myDebug("gotoPage : on change globalCurrentPage pour avoir maintenant : " + globalCurrentPage);
        //Si on arrive sur le menu on zappe toutes les autres pages possibles de la stack
        if (destination == "menu.html") {
            globalMyNavigator.resetToPage(destination, { data: { title: 'avenir', name: destination } });
        }
        else {
            if (!changeIsDone) {
                globalMyNavigator.bringPageTop(destination, { data: { title: 'avenir', name: destination } });
            }
        }
    }



    let onsleftmenu = document.getElementById('leftmenu');
    if (onsleftmenu != undefined)
        onsleftmenu.close();

    // requestKeepAlive();
    return true;
}


function cleanHistorique() {
    myDebug('call cleanHistorique');
    listeContent = new Array('');
    localStoreData("history", JSON.stringify(listeContent));
}

//Fermeture de session
function closeSession() {
    localStoreData("api_token", null);
    localStoreData("name_server", null);
    askQuitOrGoto("login.html");
}
