/*
 * (c) Éric Seigne <eric.seigne@cap-rel.fr> - 2019 - GNU AGPLv3
*/

//Ajout du message custom envoyé par le serveur sur la boite à propos
function feedAproposCustom() {
  myDebug("feedAproposCustom");
  $("#feedAproposCustom").empty();
  myDebug("feedAproposCustom empty ok -> feed with html now");

  $("#feedAproposCustom").append(localGetData('customAbout'));
  return;
}

//Ajoute quelques informations
function feedBuddyTeamLogInfos() {
  let htmlInfos = "";

  myDebug("feedBuddyTeamLogInfos");
  $("#feedBuddyTeamLogInfos").empty();
  myDebug("feedBuddyTeamLogInfos empty ok -> feed with html now");

  htmlInfos += "<p>";
  htmlInfos += "Compte " + localGetData("email") + "<br />";
  htmlInfos += "Serveur " + localGetData("name_server") + "<br />";
  htmlInfos += "Version " + globalAppVersion + "<br />";
  htmlInfos += "</p>";

  $("#feedBuddyTeamLogInfos").append(htmlInfos);
  myDebug("feedBuddyTeamLogInfos append: " + htmlInfos);
  myDebug("feedBuddyTeamLogInfos append, hide and show ok");
  return htmlInfos;
}

//Ajoute l'adresse mail du compte
function feedBuddyTeamLogAccount() {
  let htmlInfos = "";
  $("#feedBuddyTeamLogAccount").empty();
  htmlInfos += translateStrings('account') + " : " + localGetData("email");

  $("#feedBuddyTeamLogAccount").append(htmlInfos);
  myDebug("feedBuddyTeamLogAccount append: " + htmlInfos);
  return htmlInfos;
}
