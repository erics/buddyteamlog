/*
 * (c) Éric Seigne <eric.seigne@cap-rel.fr> - 2019-2021 - GNU AGPLv3
*/
// **********************************************************************
//Ne pas toucher, mis à jour automatiquement à partir du config.xml
var globalAppVersion = "0.1.0";         // mis à jour automatiquement à partir du fichier config.xml lors du build android
// **********************************************************************

var globalDevMode = true;
var globalMyNavigator = undefined;
