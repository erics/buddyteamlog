/*
 * (c) Éric Seigne <eric.seigne@cap-rel.fr> - 2019-2021 - GNU AGPLv3
*/
function loadLangFile(file, callback, errorCallback) {
    myDebug("langs : Lecture du fichier " + file);
    var rawFile = new XMLHttpRequest();
    rawFile.overrideMimeType("application/json");
    rawFile.open("GET", file, true);
    rawFile.onreadystatechange = function () {
        if (rawFile.readyState === 4 && rawFile.status == "200") {
            // myDebug("Lecture du fichier appel de callback " + callback);
            callback(rawFile.responseText);
        }
        else {
            if (rawFile.status != "200") {
                myDebug("langs : Lecture du fichier error : " + rawFile.status);
                errorCallback();
            }
        }
    };
    rawFile.onLangFileErrorLoaded = function () {
        myDebug("langs : Lecture du fichier eonLangFileErrorLoadednd, retour null");
        errorCallback();
    };
    rawFile.onerror = function () {
        myDebug("langs : Lecture du fichier onerror, retour null");
        errorCallback();
    };
    rawFile.send(null);
}

function onErrorDefaultLangLoaded() {
    myDebug("langs :  onErrorDefaultLangLoaded");
}

function onLangFileErrorLoaded() {
    myDebug("langs :  onLangFileErrorLoaded");
    //Back to default lang file
    loadLangFile("langs/en.json", onLangFileLoaded, onErrorDefaultLangLoaded);
}

function translateUI() {
    //On cherche tous les objets a traduire dans l'html
    $('.translate').each(function (i, obj) {
        myDebug("langs : traduction de " + $(obj).html());
        // alert("on traduit [" + i + "] et " + $(obj).html());
        if ($(obj).html()) {
            let t = trans($(obj).html());
            $(obj).html(t.trim());
        }
    });
}

function trans(str) {
    return translateStrings(str);
}

function onLangFileLoaded(text) {
    myDebug("langs :  onLangFileLoaded text " + text);
    if (text === null || typeof text === "undefined") {
        myDebug("langs :  onLangFileLoaded null ou indef");
    }
    else {
        var data = JSON.parse(text);
        myDebug("langs :  onLangFileLoaded ok : " + text);
        translateStrings = i18n.create({
            values: data
        });
        translateUI();
    }
}

